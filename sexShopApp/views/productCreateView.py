from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from sexShopApp.serializers.productSerializer import ProductSerializer

class ProductCreateView(views.APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
	    
        #No se necesita token
        tokenData = {"productname":request.data["productname"],
        "productcategory":request.data["productcategory"],
        "productprice":request.data["productprice"],
        "productstock":request.data["productstock"],
        "productcategory":request.data["productcategory"],
        "productdescription":request.data["productdescription"],
        "companyid":request.data["companyid"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)

        #Cada request al servidor requiere enviar el token o si el usuario ya se autenticó se puede 
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)