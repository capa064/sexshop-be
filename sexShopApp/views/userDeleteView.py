from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from sexShopApp.models.user import User
from sexShopApp.serializers.userSerializer import UserSerializer

class UserDeleteView(generics.DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)