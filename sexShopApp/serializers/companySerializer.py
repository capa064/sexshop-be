from sexShopApp.models.company import Company
from rest_framework import serializers

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['companyid', 'companyusername', 'nitrut', 'companypassword', 'companyemail', 'companyaddress', 'companyphone']