from sexShopApp.models.product import Product
from rest_framework import serializers

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['productid', 'productname', 'productcategory', 'productprice', 'productstock', 'productdescription', 'companyid']