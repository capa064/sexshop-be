from rest_framework import serializers
from sexShopApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'dateofbirth', 'usertype']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        #account = Account.objects.get(user=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'password': user.password,
            'name': user.name,
            'email': user.email,
            'dateofbirth': user.dateofbirth,
            'usertype': user.usertype,
        }