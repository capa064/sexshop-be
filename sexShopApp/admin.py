from django.contrib import admin

from .models.user import User
from .models.company import Company
from .models.product import Product
from .models.sale import Sale

admin.site.register(User)
admin.site.register(Company)
admin.site.register(Product)
admin.site.register(Sale)