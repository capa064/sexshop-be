from django.db import models
#from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
#from django.contrib.auth.hashers import make_password

class Company(models.Model):
    companyid = models.BigAutoField(primary_key=True)
    companyusername = models.CharField('Company Username', max_length = 15, unique=True)
    nitrut = models.IntegerField('Nit Rut', unique=True)
    companypassword = models.CharField('Company Password', max_length = 256)
    companyemail = models.EmailField('Company Email', max_length = 100)
    companyaddress = models.CharField('Company Address', max_length = 100)
    companyphone = models.IntegerField('Company Phone', unique=True)