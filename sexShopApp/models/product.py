from django.db import models
#from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager 
#from django.contrib.auth.hashers import make_password

from sexShopApp.models.company import Company

class Product(models.Model):
    productid = models.BigAutoField(primary_key=True)
    productname = models.CharField('Product Name', max_length = 15, unique=True)
    productcategory = models.CharField('Product Category', max_length = 15, unique=True)
    productprice = models.IntegerField('Product Price')
    productstock = models.IntegerField('Product Stock')
    productdescription = models.CharField('Product Description', max_length=200)
    companyid = models.ForeignKey(Company, related_name='Product', on_delete=models.CASCADE)