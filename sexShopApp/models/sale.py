from django.db import models
#from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, User 
#from django.contrib.auth.hashers import make_password

from sexShopApp.models.company import Company
from sexShopApp.models.user import User

class Sale(models.Model):
    receipt = models.BigAutoField(primary_key=True)
    saledate = models.DateField('Sale Date', max_length=15)
    productid = models.ForeignKey(Company, related_name='Sale', on_delete=models.CASCADE)
    userid = models.ForeignKey(User, related_name='Sale', on_delete=models.CASCADE)